// Гравитация
(function () {
    qwords = JSON.parse(prompt('Вставь сюда JSON данные из скрипта "Карточки":'));

    function qwrite() {
        if (document.querySelector('.GravityTerm-text') &&
            document.querySelector('.GravityTypingPrompt-input').textContent.length <= 1) {
            console.log('%cSUCCESS EVENT STARTED!', 'background: #222; color: #bada55');
            qword = '';
            qwordexpl = document.querySelector('.GravityTerm-text').textContent;

            qwords.forEach(function (item, i, arr) {
                if (item['0'] == qwordexpl) {
                    qword = item['1'];
                } else if (item['1'] == qwordexpl) {
                    qword = item['0'];
                }
            });

            if (qword) {
                qword += '♥';

                document.querySelector('.GravityTypingPrompt-input').value = qword;
                document.querySelector('.GravityTypingPrompt-input').textContent = qword;

                qwrite();
            }
        } else {
            console.log('%cTRYING TO EVENT REGISTER!', 'background: #222; color: #ffda55');
            setTimeout(() => {
                try {
                    qwrite();
                } catch (error) {

                }
            }, 500);
        }
    }
    qwrite();
})();
