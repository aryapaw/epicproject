// Заучивание
(function () {
    qwords = JSON.parse(prompt('Вставь сюда JSON данные из скрипта "Карточки":'));

    function qwrite() {
        if (document.querySelector('.FormattedTextWithImage > .FormattedText > div')) {

            if (document.querySelectorAll('.MultipleChoiceQuestionPrompt-termOptions > .MultipleChoiceQuestionPrompt-termOption').length > 1) {
                console.log('%cSUCCESS EVENT STARTED!', 'background: #222; color: #bada55');
                qword = '';
                qwordexpl = document.querySelector('.FormattedTextWithImage > .FormattedText > div').innerHTML;

                document.querySelector('.MultipleChoiceQuestionPrompt-termOptions > .MultipleChoiceQuestionPrompt-termOption')

                qwords.forEach(function (item, i, arr) {
                    if (item['0'] == qwordexpl) {
                        qword = item['1'];
                    } else if (item['1'] == qwordexpl) {
                        qword = item['0'];
                    }
                });


                if (qword) {
                    qbuttons = document.querySelectorAll('.MultipleChoiceQuestionPrompt-termOptions > .MultipleChoiceQuestionPrompt-termOption');

                    for (let index = 0; index < qbuttons.length; index++) {
                        qbutton = qbuttons[index].querySelector('.MultipleChoiceQuestionPrompt-termOptionInner > .FormattedTextWithImage > .FormattedText');
                        if (qbutton.innerHTML == qword) qbutton.click();
                    }

                    setTimeout(() => {
                        qwrite();
                    }, 1500);
                }
            } else {
                if (document.querySelector('.AutoExpandTextarea-textarea').textContent.length <= 1) {
                    console.log('%cSUCCESS EVENT STARTED!', 'background: #222; color: #bada55');
                    qword = '';
                    qwordexpl = document.querySelector('.FormattedTextWithImage > .FormattedText > div').innerHTML;

                    qwords.forEach(function (item, i, arr) {
                        if (item['0'] == qwordexpl) {
                            qword = item['1'];
                        } else if (item['1'] == qwordexpl) {
                            qword = item['0'];
                        }
                    });

                    if (qword) {
                        qword += '♥';

                        document.querySelector('.AutoExpandTextarea-textarea').value = qword;
                        document.querySelector('.AutoExpandTextarea-textarea').textContent = qword;

                        setTimeout(() => {
                            qwrite();
                        }, 500);
                    }
                } else {
                    qTryEventRegister();
                }
            }

        } else {
            qTryEventRegister();
        }
    }

    function qTryEventRegister() {
        console.log('%cTRYING TO EVENT REGISTER!', 'background: #222; color: #ffda55');
        setTimeout(() => {
            qwrite();
        }, 500);
    }

    qwrite();
})();